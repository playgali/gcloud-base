FROM debian:stretch-slim
MAINTAINER Galileo Martinez "playgali@gmail.com"
ENV REFRESHED_AT 2017-Nov-22

ENV PATH "$PATH:/opt/google-cloud-sdk/bin"
RUN apt-get update && apt-get install curl python -y \
 && curl https://sdk.cloud.google.com | bash -s -- --disable-prompts --install-dir=/opt \
 && /opt/google-cloud-sdk/bin/gcloud components install kubectl --quiet \
 && curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get | tac | tac | bash
